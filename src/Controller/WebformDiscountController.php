<?php

namespace Drupal\webform_discount\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\webform_discount\Entity\WebformDiscountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class WebformDiscountController.
 *
 *  Returns responses for Webform discount routes.
 */
class WebformDiscountController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a Webform discount revision.
   *
   * @param int $webform_discount_revision
   *   The Webform discount revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($webform_discount_revision) {
    $webform_discount = $this->entityTypeManager()->getStorage('webform_discount')
      ->loadRevision($webform_discount_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('webform_discount');

    return $view_builder->view($webform_discount);
  }

  /**
   * Page title callback for a Webform discount revision.
   *
   * @param int $webform_discount_revision
   *   The Webform discount revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($webform_discount_revision) {
    $webform_discount = $this->entityTypeManager()->getStorage('webform_discount')
      ->loadRevision($webform_discount_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $webform_discount->label(),
      '%date' => $this->dateFormatter->format($webform_discount->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Webform discount.
   *
   * @param \Drupal\webform_discount\Entity\WebformDiscountInterface $webform_discount
   *   A Webform discount object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(WebformDiscountInterface $webform_discount) {
    $account = $this->currentUser();
    $webform_discount_storage = $this->entityTypeManager()->getStorage('webform_discount');

    $build['#title'] = $this->t('Revisions for %title', ['%title' => $webform_discount->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all webform discount revisions") || $account->hasPermission('administer webform discount entities')));
    $delete_permission = (($account->hasPermission("delete all webform discount revisions") || $account->hasPermission('administer webform discount entities')));

    $rows = [];

    $vids = $webform_discount_storage->revisionIds($webform_discount);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\webform_discount\WebformDiscountInterface $revision */
      $revision = $webform_discount_storage->loadRevision($vid);
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $webform_discount->getRevisionId()) {
          $link = $this->l($date, new Url('entity.webform_discount.revision', [
            'webform_discount' => $webform_discount->id(),
            'webform_discount_revision' => $vid,
          ]));
        }
        else {
          $link = $webform_discount->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => Url::fromRoute('entity.webform_discount.revision_revert', [
                'webform_discount' => $webform_discount->id(),
                'webform_discount_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.webform_discount.revision_delete', [
                'webform_discount' => $webform_discount->id(),
                'webform_discount_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
    }

    $build['webform_discount_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
