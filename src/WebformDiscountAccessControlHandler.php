<?php

namespace Drupal\webform_discount;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Webform discount entity.
 *
 * @see \Drupal\webform_discount\Entity\WebformDiscount.
 */
class WebformDiscountAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\webform_discount\Entity\WebformDiscountInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished webform discount entities');
        }


        return AccessResult::allowedIfHasPermission($account, 'view published webform discount entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit webform discount entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete webform discount entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add webform discount entities');
  }


}
