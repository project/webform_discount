<?php

namespace Drupal\webform_discount\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Webform discount entities.
 *
 * @ingroup webform_discount
 */
interface WebformDiscountInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Webform discount name.
   *
   * @return string
   *   Name of the Webform discount.
   */
  public function getName();

  /**
   * Sets the Webform discount name.
   *
   * @param string $name
   *   The Webform discount name.
   *
   * @return \Drupal\webform_discount\Entity\WebformDiscountInterface
   *   The called Webform discount entity.
   */
  public function setName($name);

  /**
   * Gets the Webform discount creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Webform discount.
   */
  public function getCreatedTime();

  /**
   * Sets the Webform discount creation timestamp.
   *
   * @param int $timestamp
   *   The Webform discount creation timestamp.
   *
   * @return \Drupal\webform_discount\Entity\WebformDiscountInterface
   *   The called Webform discount entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Webform discount revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Webform discount revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\webform_discount\Entity\WebformDiscountInterface
   *   The called Webform discount entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Webform discount revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Webform discount revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\webform_discount\Entity\WebformDiscountInterface
   *   The called Webform discount entity.
   */
  public function setRevisionUserId($uid);

  /**
   * Get status
   *
   * @return bool
   */
  public function getStatus();

  /**
   * @return mixed
   */
  public function getDescription();

  /**
   * @return mixed
   */
  public function getCode();

  /**
   * @return mixed
   */
  public function getDiscountFixed();

  /**
   * @return mixed
   */
  public function getDiscountPercent();

  /**
   * @return mixed
   */
  public function getExpiration();

  /**
   * @return mixed
   */
  public function getUsageLimit();

  /**
   * Apply the discounts. This will check for the existance of a percentage discount
   * first, and apply that. If percentage discount is not set, it will then look
   * check for a fixed discount and apply that.
   *
   * @param float $number
   * @return float
   */
  public function applyDiscount($number);

}
