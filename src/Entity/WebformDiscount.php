<?php

namespace Drupal\webform_discount\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Webform discount entity.
 *
 * @ingroup webform_discount
 *
 * @ContentEntityType(
 *   id = "webform_discount",
 *   label = @Translation("Webform discount"),
 *   handlers = {
 *     "storage" = "Drupal\webform_discount\WebformDiscountStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\webform_discount\WebformDiscountListBuilder",
 *     "views_data" = "Drupal\webform_discount\Entity\WebformDiscountViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\webform_discount\Form\WebformDiscountForm",
 *       "add" = "Drupal\webform_discount\Form\WebformDiscountForm",
 *       "edit" = "Drupal\webform_discount\Form\WebformDiscountForm",
 *       "delete" = "Drupal\webform_discount\Form\WebformDiscountDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\webform_discount\WebformDiscountHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\webform_discount\WebformDiscountAccessControlHandler",
 *   },
 *   base_table = "webform_discount",
 *   revision_table = "webform_discount_revision",
 *   revision_data_table = "webform_discount_field_revision",
 *   translatable = FALSE,
 *   admin_permission = "administer webform discount entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/webform_discount/{webform_discount}",
 *     "add-form" = "/admin/structure/webform_discount/add",
 *     "edit-form" = "/admin/structure/webform_discount/{webform_discount}/edit",
 *     "delete-form" = "/admin/structure/webform_discount/{webform_discount}/delete",
 *     "version-history" = "/admin/structure/webform_discount/{webform_discount}/revisions",
 *     "revision" = "/admin/structure/webform_discount/{webform_discount}/revisions/{webform_discount_revision}/view",
 *     "revision_revert" = "/admin/structure/webform_discount/{webform_discount}/revisions/{webform_discount_revision}/revert",
 *     "revision_delete" = "/admin/structure/webform_discount/{webform_discount}/revisions/{webform_discount_revision}/delete",
 *     "collection" = "/admin/structure/webform_discount",
 *   },
 *   field_ui_base_route = "webform_discount.settings"
 * )
 */
class WebformDiscount extends EditorialContentEntityBase implements WebformDiscountInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    // If no revision author has been set explicitly,
    // make the webform_discount owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $weight = 0;


    // Name
    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Webform discount entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string ',
        'weight' => $weight,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $weight++;

    // The actual discount code
    $fields['code'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Discount code'))
      ->setDescription(t('The string which will be used to identify this discount code.'))
      ->setRevisionable(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue('')
      ->setSettings([
        'max_length' => 50,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => $weight,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => $weight,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $weight++;

    // Description
    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Description'))
      ->setDescription(t('Description of this code. Include any notes here. These will not be displayed externally.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => $weight,
        'settings' => [
          'rows' => 5,
        ]
      ]);

    $weight++;

    // Fixed amount to deduct.
    $fields['discount_fixed'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Fixed discount amount'))
      ->setDescription(t('Amount that will be deducted from the total. If the deduction exceeds the total amount, then that amount will be set to zero. Leave blank if using a percentage instead.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => $weight,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => $weight,
      ]);

    $weight++;

    // Precentage amount to deduct.
    $fields['discount_percent'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Percent discount amount'))
      ->setDescription(t('A percentage to deduct from an amount. Enter whole numbers between 0 and 100.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => $weight,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => $weight,
      ]);

    $weight++;

    // Expiration date of discount.
    $fields['expiration'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Expiration'))
      ->setDescription(t('Date after which this code will stop being used.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'datetime',
        'weight' => $weight,
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime',
        'weight' => $weight,
      ]);

    $weight++;

    // Limit of uses
    $fields['usage_limit'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Usage limit'))
      ->setDescription(t('Maximum number of times a discount code may be used.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'integer',
        'weight' => $weight,
      ])
      ->setDisplayOptions('form', [
        'type' => 'integer',
        'weight' => $weight,
      ]);

    $weight++;

    // Created
    $fields['created'] = BaseFieldDefinition::create('created')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'))
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'hidden',
        'weight' => $weight,
      ])
      ->setDisplayOptions('form', [
        'type' => 'hidden',
        'weight' => $weight,
      ]);


    // Changed
    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the discount code was last edited.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);


    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Webform discount entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => $weight,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => $weight,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $weight++;


    // Status field,
    $fields['status']->setDescription(t('A boolean indicating whether the Webform discount is enabled.'))
      ->setLabel(t('Enabled'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => $weight,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => $weight,
      ]);

    $weight++;


    return $fields;
  }


  /**
   * Get status
   *
   * @return bool
   */
  public function getStatus(){
    return $this->get('status')->value;
  }

  /**
   * @return mixed
   */
  public function getDescription(){
    return $this->get('description')->value;
  }

  /**
   * @return mixed
   */
  public function getCode(){
    return $this->get('code')->value;
  }

  /**
   * @return mixed
   */
  public function getDiscountFixed(){
    return $this->get('discount_fixed')->value;
  }

  /**
   * @return mixed
   */
  public function getDiscountPercent(){
    return $this->get('discount_percent')->value;
  }

  /**
   * @return mixed
   */
  public function getExpiration(){
    return $this->get('expiration')->value;
  }

  /**
   * @return mixed
   */
  public function getUsageLimit(){
    return $this->get('usage_limit')->value;
  }

  /**
   * @inheritDoc
   */
  public function applyDiscount($number){
    if ($this->getDiscountPercent()) {
      return $this->applyPercentDiscount($number);
    }
    else if ($this->getDiscountFixed()) {
      return $this->applyFixedDiscount($number);
    }
    else {
      throw new Exception(t('Discount entity does not have a percentage or a fixed value. It must have one or the other.'));
    }
  }

  /**
   * helper to apply discount based on percentage.
   *
   * @param $number
   */
  private function applyPercentDiscount($number) {
    $percent = $this->getDiscountPercent();
    // Percentages are stored as an integer between one and 100;
    // but probably should store as a float between 0 and 1.
    $result = ((100 - $percent) / 100) * $number;
    // Not really necessaryhere.
    return $this->floorZero($result);
  }

  /**
   * Helper to apply discount based on a fixed value.
   *
   * @param $number
   */
  private function applyFixedDiscount($number) {
    $fixed = (float) $this->getDiscountFixed();
    $result = $number - $fixed;
    return $this->floorZero($result);
  }

  /**
   * Helper to ensure if the resulting discount amount is a negative number, we
   * return zero instead.
   *
   * @param $number
   * @return int
   */
  private function floorZero($number) {

    if ($number >= 0) {
      return $number;
    }
    else {
      return 0;
    }
  }

}
