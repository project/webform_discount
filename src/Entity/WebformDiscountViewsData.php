<?php

namespace Drupal\webform_discount\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Webform discount entities.
 */
class WebformDiscountViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
