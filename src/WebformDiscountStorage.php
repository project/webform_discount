<?php

namespace Drupal\webform_discount;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\webform_discount\Entity\WebformDiscountInterface;

/**
 * Defines the storage handler class for Webform discount entities.
 *
 * This extends the base storage class, adding required special handling for
 * Webform discount entities.
 *
 * @ingroup webform_discount
 */
class WebformDiscountStorage extends SqlContentEntityStorage implements WebformDiscountStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(WebformDiscountInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {webform_discount_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {webform_discount_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

}
