<?php


namespace Drupal\webform_discount\Plugin\WebformHandler;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Component\Utility\Html;
use Drupal\facets\Exception\Exception;
use Drupal\webform\Plugin\WebformElement\BooleanBase;
use Drupal\webform\Plugin\WebformElementManagerInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformInterface;
use Drupal\webform\WebformSubmissionConditionsValidatorInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform\WebformHandlerInterface;
use Drupal\webform\Element\WebformAjaxElementTrait;
use \Drupal\node\Entity\Node;
use Drupal\webform_discount\Entity;
use Drupal\webform\WebformTokenManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\webform\Utility\WebformFormHelper;



/**
 * Set session time value for a webform submission.
 *
 * @WebformHandler(
 *   id = "Webform discount",
 *   label = @Translation("Webform discount"),
 *   category = @Translation("Webform"),
 *   description = @Translation("Webform discount."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_REQUIRED,
 * )
 */
class WebformDiscountWebformHandler  extends WebformHandlerBase {


  use WebformAjaxElementTrait;


  /**
   * The webform element plugin manager.
   *
   * @var \Drupal\webform\Plugin\WebformElementManagerInterface
   */
  protected $elementManager;

  const WEBFORM_DISCOUNT_CONFIRM = 'webform_discount_submit';

  protected $flat;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory'),
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('webform_submission.conditions_validator'),
      $container->get('database'),
      $container->get('webform.token_manager'),
      $container->get('plugin.manager.webform.element')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerChannelFactoryInterface $logger_factory, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, WebformSubmissionConditionsValidatorInterface $conditions_validator, Connection $database, WebformTokenManagerInterface $token_manager, WebformElementManagerInterface $element_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $logger_factory, $config_factory, $entity_type_manager, $conditions_validator);
    $this->database = $database;
//    $this->tokenManager = $token_manager;
    $this->elementManager = $element_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'element_key' => '',
      'element_target' => '',
      'use_ajax_validation' => TRUE,
      'ajax_validation_button_text' => 'Check for discounts',
      'ajax_validation_confirm_text' => 'Discount applied!',
      'ajax_validation_fail_text' => 'That code was not valid.',
      // TODO: Add configuration: Use ajax validatin, and ajax validate button text,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $configuration = $this->getConfiguration();
    $settings = $configuration['settings'];

    $element = $this->getWebform()->getElement($settings['element_key']);
    if ($element) {
      $webform_element = $this->elementManager->getElementInstance($element);
      $pluginLabel = $webform_element->getPluginLabel();
      $t_args = [
        '@title' => $webform_element->getAdminLabel($element),
        '@type' => $pluginLabel,
      ];
      $settings['element_key'] = $this->t('@title (@type)', $t_args);
    }
    elseif (empty($settings['element_key'])) {
      $settings['element_key'] = [
        '#type' => 'link',
        '#title' => $this->t('Please add a new options or checkbox element.'),
        '#url' => $this->getWebform()->toUrl('edit-form'),
      ];
    }
    else {
      $settings['element_key'] = [
        '#markup' => $this->t("'@element_key' is missing.", ['@element_key' => $settings['element_key']]),
        '#prefix' => '<b class="color-error">',
        '#suffix' => '</b>',
      ];
    }

    return [
        '#settings' => $settings,
      ] + parent::getSummary();
  }

  /**
   * @inheritDoc
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $this->applyFormStateToConfiguration($form_state);

    // Element settings.
    $form['element_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Element settings'),
      '#open' => TRUE,
    ];
    $form['element_settings']['element_key'] = [
      '#type' => 'select',
      '#title' => $this->t('Element'),
      '#description' => $this->t('Field which will contain a discount code, this will be compared with the code in Webform Discount entities.'),
      '#options' => $this->getElements(),
      '#default_value' => !empty($this->configuration['element_key']) ? $this->configuration['element_key'] : '',
      '#required' => TRUE,
      '#empty_option' => (empty($this->configuration['element_key'])) ? $this->t('- Select -') : NULL,
    ];

    $form['element_settings']['element_target'] = [
      '#type' => 'select',
      '#title' => $this->t('Target element'),
      '#description' => $this->t('Field to apply discounts, this should be a number field which will have the matching discount code operations applied to.'),
      '#options' => $this->getTargetElements(),
      '#default_value' => !empty($this->configuration['element_key']) ? $this->configuration['element_key'] : '',
      '#required' => TRUE,
      '#empty_option' => (empty($this->configuration['element_key'])) ? $this->t('- Select -') : NULL,
    ];

    $form['element_settings']['ajax_validation_button_text'] = [
      '#type' => 'text',
      '#title' => $this->t('Validation button text'),
      '#description' => $this->t(''),
      '#default_value' => !empty($this->configuration['ajax_validation_button_text']) ? $this->configuration['ajax_validation_button_text'] : '',
      '#required' => TRUE,
    ];

    $form['element_settings']['ajax_validation_confirm_text'] = [
      '#type' => 'text',
      '#title' => $this->t('Confirmation text'),
      '#description' => $this->t(''),
      '#default_value' => !empty($this->configuration['ajax_validation_confirm_text']) ? $this->configuration['ajax_validation_confirm_text'] : '',
      '#required' => TRUE,
    ];

    $form['element_settings']['ajax_validation_fail_text'] = [
      '#type' => 'text',
      '#title' => $this->t('Failure text'),
      '#description' => $this->t(''),
      '#default_value' => !empty($this->configuration['ajax_validation_fail_text']) ? $this->configuration['ajax_validation_fail_text'] : '',
      '#required' => TRUE,
    ];

    return $this->setSettingsParents($form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration = $this->defaultConfiguration();
    parent::submitConfigurationForm($form, $form_state);
    $this->applyFormStateToConfiguration($form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function alterForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    $discountKey = $this->getCodeElementKey();
    $targetKey = $this->getTargetElementKey();
    $inputs = $form_state->getUserInput();
    $this->flat = WebformFormHelper::flattenElements($form);
    // This was initially set up to use a "Button" element to trigger ajax
    // validation, however, due to limitations in Drupal core at the time, this
    // was replaced with adding theajax directly on the discount code element.
    // TODO: Re-add this feature once linked issues in
    // https://www.drupal.org/project/webform_discount/issues/3171142 are
    // resolved.
    $useAjaxButton = false;

    // Alter the form to add some ajax validation functionality.

    if ($this->configuration['use_ajax_validation']) {
      if ($useAjaxButton) {
        // Insert the ajax button, locked out for now.
        $this->insertAjaxButton($form, $form_state);
      }
      else {
        // add ajax to the element itself.
        $this->addAjaxToInputs($form, $form_state);
      }
    }

//return;
    // What would be in the ajax callback, but we just do everything in the
    // form re-generation.
    // Still need to address the form id being incorrect on re-generation...
    // This doesn't seem to do anything?
    if (isset($inputs[$discountKey]) && isset($this->flat[$discountKey])){
      // Get the matching discount entities.

    }
  }


  private function addAjaxToInputs(array &$form, FormStateInterface $form_state) {
    $discountKey = $this->getCodeElementKey();
    $targetKey = $this->getTargetElementKey();
    $form_id = $this->getFormId($form, $form_state);
    $form['#id'] = $form_id;

    $suffixArray = [
      [
        [
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#attributes' => [
            'class' => ['form-item--discount-code-status'],
            'id' => 'discount-code-status',
          ],
          '#value' => '',
        ],
      ]
    ];
    $renderer = \Drupal::service('renderer');
    $html = $renderer->render($suffixArray);

    $this->flat[$discountKey]['#ajax'] = [
      'event' => 'blur',
      'progress' => [
        'type' => 'throbber',
        // This is why we customize this, Show no text to reduce pushing the button.
        'message' => '',
      ],
      'disable-refocus' => TRUE, // Avoids locking the curser into the element.
      'callback' => [$this, 'validateDiscountCode'],
      'method' => 'replaceWith',
      'wrapper' => $form_id,
    ];
    $this->flat[$discountKey]['#suffix'] = $html;

    $this->flat[$discountKey]['#attached'] = [
      'library' => [
        'webform_discount/webform_discount.theme'
      ]
    ];
    $this->flat[$discountKey]['#attributes'] = [
      'class' => [
        'webform-discount'
      ],
    ];

  }

  /**
   * Inserts the button element to trigger ajax behavior. This isn't used now as
   * Drupal core does not support ajax on buttons.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  private function insertAjaxButton(array &$form, FormStateInterface $form_state) {
    $discountKey = $this->getCodeElementKey();
    $targetKey = $this->getTargetElementKey();
    $inputs = $form_state->getUserInput();


    if ($form_state->getBuildInfo()['form_id']
      && (empty($_POST) || (isset($_GET['ajax_form']) && $_GET['ajax_form'] === '1'))
    ) {
      $form_id = $this->getFormId($form, $form_state);
      $form['#id'] = $form_id;
      $discountCodeElement =& $this->flat[$discountKey];
      if (isset($this->flat[$discountKey])
        && $discountCodeElement['#webform_parent_key'] === '' ) {
        $this->insertAjaxValidator($form, $form_state);
      }
      else if(!empty($discountCodeElement)
        && $discountCodeElement['#webform_parent_key'] !== '') {
        // Couldn't find the element in the top level. So no we need to look for it in containers.
        // Will this work, passing a sub array by reference?
        $this->insertAjaxValidator($this->flat[$discountCodeElement['#webform_parent_key']], $form, $form_state);
      }
    }


  }

  /**
   * @param $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  private function insertAjaxValidator(&$form, FormStateInterface $form_state) {
    $discountKey = $this->getCodeElementKey();
    $ajaxValidator = $this->createAjaxValidator($form['#id']);
    $isRoot = FALSE;
    if ($this->flat[$discountKey]['#webform_parent_key'] === '') {
      $isRoot = TRUE;
      $index = array_flip(array_keys($form['elements']))[$discountKey];
    }
    else {
      $index = array_flip(array_keys($this->flat))[$discountKey];
    }
    $index++;
    // Insert the button after the Discount code field.
    // array_splice($form['elements'], $index, 0, $ajaxValidator);
    // Instead of the commented out line, use array_slice to insert a named key.
    // Check for existing presence of ajax element in case this is rebuiding
    // the form after the ajax callback.

    // I hate not being DRY, but This needs to be handled differently depending
    // on whether the discount code element is nested in a parent container, or
    // is at the root of the form.
    if ($isRoot) {
      if(!isset($form['elements'][$this->getWebformDiscountConfirmKey()])) {
        $form['elements'] = array_slice($form['elements'], 0, $index, true) +
          [$this->getWebformDiscountConfirmKey() => $ajaxValidator] +
          array_slice($form['elements'], $index, NULL, true);
      }
    }
    else {
      if(!isset($this->flat[$this->getWebformDiscountConfirmKey()])) {
        $this->flat = array_slice($this->flat, 0, $index, true) +
          [$this->getWebformDiscountConfirmKey() => $ajaxValidator] +
          array_slice($this->flat, $index, NULL, true);
      }
    }
  }


  /**
   * Generates ajax element to be inserted in the form.
   *
   * @param $form_id
   * @return array
   */
  public function createAjaxValidator($form_id) {
    return [
      '#type' => 'button',
      '#value' => $this->t($this->configuration['ajax_validation_button_text']),
      '#ajax'=> [
        'callback' => [$this, 'validateAjaxForm'],
        'method' => 'replaceWith',
        'wrapper' => $form_id,
      ],
      '#attached'=> [
        'library' => [
          'webform_discount/webform_discount.theme'
        ]
      ],
      '#attributes' => [
        'class' => [
          'webform-discount'
        ],
      ],
    ];
  }

  /**
   * Determins the form id of a form.
   *
   * @param $form
   * @param $form_state
   */
  public function getFormId($form, $form_state) {
    // Sometimes it should be $form['form_build_id'], but why?

    if ($form_state->getCompleteForm()) {
      $completeForm = $form_state->getCompleteForm();
      $form_id = $completeForm['#id'];
    }
    elseif (isset($form['#build_id'])) {
      $form_id = $form['#build_id'];
    }
    elseif($form_state->getBuildInfo()['form_id']) {
      // THis gets used on initial page load.
      $form_id = $form_state->getBuildInfo()['form_id'];
    }

    // We need access to $form['#id] now, but that is not generated until later
    // in FormBuilder::prepareForm(). But we can set it now, and prepareForm()
    // will just use that.
    // We do this because ajax requests will replace the form id with new ones
    // based on a random string.
    if ($form_id) {
      return Html::getUniqueId($form_id);
    }
    // why is this necessary?
    // The form id uses underscores here, but gets rendered with hyphens on the page.
    return str_replace('_', '-', $form_id);

  }


  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    // Get the discount code used.
    $key = $this->getCodeElementKey();
    $input = $form_state->getUserInput();
    $code = $input[$key];
    // Don't validate empty values. They are simply not using a code.
    if(empty($code)) {
      return;
    }
    // Use it in entity query
    $discounts = $this->getDiscounts($code);
    // Validate
    if (!empty($discounts)) {
      // Do nothing?
    }
    else {
      // Set error because of invalid discount code.
      $form_state->setErrorByName($key, $this->t('The entered discount code is not valid.'));
    }
  }


  /**
   * This was an expirament...
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @return \Drupal\Core\Ajax\AjaxResponse
   * @throws \Exception
   */
  public function validateDiscountCode(array &$form, FormStateInterface $form_state)  {
    $this->flat = WebformFormHelper::flattenElements($form);
    $response = new AjaxResponse();
    $discountKey = $this->getCodeElementKey();
    $targetKey = $this->getTargetElementKey();
    $inputs = $form_state->getUserInput();
    $classes = ['form-item--discount-code-status'];
    $discounts = $this->getDiscounts($inputs[$discountKey]);
    if (!empty($discounts)) {
      // Just use the first matching one.
      $discount = array_shift($discounts);
      if ($this->flat[$targetKey]['#placeholder'] === NULL) {
        throw new \Exception('Placeholder value is not set on the target element: ' . $targetKey);
      }
      // Use placeholder text, because #default_value changes as discounts are
      // applied when the form is submitted.
      $defaultValue = $this->flat[$targetKey]['#placeholder'];
      $defaultValue = (float) $defaultValue;
      if (!is_numeric($defaultValue)) {
        throw new \Exception('Default value on the target element is not numeric: ' . $targetKey . ', value: ' . $this->flat[$targetKey]['#default_value']);
      }
      $result = $this->applyDiscount($defaultValue, $discount);
      if ($result) {
        $this->flat[$targetKey]['#value'] = $result;
        // This forces the integer field to display two decimal places.
        // TODO: Don't run this when the form is actually being submitted.
        $classes[] = 'form-item--discount-code-status--success';
        $responseText = $this->t($this->getAjaxConfirmText());
        $response->addCommand(
          // TODO Find a more reliably way to predict the id.
          new InvokeCommand('#edit-' . $targetKey, 'val', [$result])
        );
      }
    }
    else if($inputs[$discountKey] !== '') {
      // Provide indication that the code was not valid.
      $responseText = $this->t($this->getAjaxFailText());
      $classes[] = 'form-item--discount-code-status--fail';
      $this->insertDiscountFailedText($form);
    }

    //    $this->insertStatusText($validationText,$form, '');

    if (isset($responseText)) {
      $response
        ->addCommand(new ReplaceCommand('#discount-code-status', '<span id="discount-code-status" class="'.implode(' ', $classes).'">' . $responseText . '</span>'));
    }

    return $response;
  }

  /**
   * This is not used
   * Ajax callback for discount code validation.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @return array
   */
  public function validateAjaxForm(array &$form, FormStateInterface $form_state) {
    // TODO: Is any of this still necessary, since we take care of most things in alterForm()?

    $discountKey = $this->getCodeElementKey();
    $targetKey = $this->getTargetElementKey();
    $inputs = $form_state->getUserInput();

    // This doesn't seem to do anything?
    if (isset($inputs[$discountKey])){
      $discounts = $this->getDiscounts($inputs[$discountKey]);
      if (!empty($discounts)) {
        // Just use the first matching one.
        $discount = array_shift($discounts);
        if (isset($this->flat[$targetKey])
          && isset($this->flat[$targetKey]['#default_value'])) {
          $defaultValue = $this->flat[$targetKey]['#default_value'];
        }
        else {
          $defaultValue = '';
        }
        $result = $this->applyDiscount($defaultValue, $discount);
        }
      }

    // Now we need to update the id of the ajax element, because it gets
    // replaced when the form is regenerated.
    $form_id = $this->getFormId($form, $form_state);
    $this->flat[$this->getWebformDiscountConfirmKey()]['#ajax']['wrapper'] = $form_id;

    // Probably need to trigger a rebuild because we've added new ajax settings.
    return $form;
  }

  /**
   * Inserts the confirmation text when a discount is successfully applied
   *
   * @param array $form (flattened version as returned from \Drupal\webform\Utility\WebformFormHelper::flattenElements())
   */
  private function insertDiscountConfirmationText(array &$form) {
    $validationText = $this->t($this->getAjaxConfirmText());
    $this->insertStatusText($validationText,$form, 'form-item--discount-code-status--success');
  }

  /**
   * Inserts the confirmation text when a discount is successfully applied
   *
   * @param array $form (flattened version as returned from \Drupal\webform\Utility\WebformFormHelper::flattenElements())
   */
  private function insertDiscountFailedText(array &$form) {
    $validationText = $this->t($this->getAjaxFailText());
    $this->insertStatusText($validationText,$form, 'form-item--discount-code-status--fail');
  }

  /**
   * Inserts the provided text adjacent to the discount code element.
   *
   * @param $text
   * @param $form
   */
  private function insertStatusText($text, &$form, $class = 'status') {
    $key = $this->getWebformDiscountConfirmKey();
    $formPosition =  '#suffix';
    $renderer = \Drupal::service('renderer');

    $element = [
      '#type' => 'html_tag',
      '#tag' => 'span',
      '#attributes' => [
        'class' => ['form-item--discount-code-status', $class],
      ],
      '#value' => $text,
    ];

    $result = $renderer->render($element);

    $this->flat[$key][$formPosition]= $result;
  }

  /**
   * {@inheritDoc}
   */
  public function preSave(WebformSubmissionInterface $webform_submission) {
    $key = $this->getCodeElementKey();
    $targetKey = $this->getTargetElementKey();
    $values = $webform_submission->getData();
    $discounts = $this->getDiscounts($values[$key]);
    if ($discounts) {
      // Just use the first one.
      // TODO: Figure out if this would ever be a use case.
      $discount = array_shift($discounts);
      // Apply any discounts?
      $discountedValue = $this->applySubmissionDiscount($webform_submission, $discount);
      $webform_submission->setElementData($targetKey, $discountedValue);
    }
  }

  /**
   * Get key/value array of webform options or boolean elements.
   *
   * @return array
   *   A key/value array of webform options or boolean elements.
   */
  protected function getElements() {
    $webform = $this->getWebform();
    $elements = $webform->getElementsInitializedAndFlattened();

    $options = [];
    foreach ($elements as $element) {
      $webform_element = $this->elementManager->getElementInstance($element);
      // TODO: This is probably not safe. Find way to reference the specific field type.
      if ( $webform_element->getPluginLabel() == 'Text field' ) {
        $webform_key = $element['#webform_key'];
        $t_args = [
          '@title' => $webform_element->getAdminLabel($element),
          '@type' => $webform_element->getPluginLabel(),
        ];
        $options[$webform_key] = $this->t('@title (@type)', $t_args);
      }
    }
    return $options;
  }

  /**
   * Same as above for getting target element, which should be a number field.
   * TODO: Make this more DRY.
   *
   * @return array
   * @throws \Exception
   */
  protected function getTargetElements() {
    $webform = $this->getWebform();
    $elements = $webform->getElementsInitializedAndFlattened();
    $options = [];
    foreach ($elements as $element) {
      $webform_element = $this->elementManager->getElementInstance($element);
      // TODO: This is probably not safe. Find way to reference the specific field type.
      if ( $webform_element->getPluginLabel() == 'Number' ) {
        $webform_key = $element['#webform_key'];
        $t_args = [
          '@title' => $webform_element->getAdminLabel($element),
          '@type' => $webform_element->getPluginLabel(),
        ];
        $options[$webform_key] = $this->t('@title (@type)', $t_args);
      }
    }

    return $options;
  }

  ////////////////////////////////////////
  //  Webform Discount specific methods //
  ////////////////////////////////////////

  /**
   * Gets the key for the discount code element identified in the handler
   * configuration.
   * @return string
   */
  protected function getCodeElementKey() {
    $config = $this->getConfiguration();
    return $config['settings']['element_key'];
  }

  /**
   * Gets the key for the target element that discounts will be applied to.
   *
   * @return string
   */
  protected function getTargetElementKey() {
    $config = $this->getConfiguration();
    return $config['settings']['element_target'];
  }

  /**
   * Gets the text to be displayed to the user when a discount code is successful.
   *
   * @return string
   */
  protected function getAjaxConfirmText() {
    $config = $this->getConfiguration();
    return $config['settings']['ajax_validation_confirm_text'];
  }

  /**
   * Gets the text to be displayed to the user when a discount code is fails.
   *
   * @return string
   */
  protected function getAjaxFailText() {
    $config = $this->getConfiguration();
    return $config['settings']['ajax_validation_fail_text'];
  }

  /**
   * @return string
   */
  public function getWebformDiscountConfirmKey() {
    return $this::WEBFORM_DISCOUNT_CONFIRM;
  }

  /**
   * Look up matching discount entities for a given code.
   */
  protected function getDiscounts($code){
    $now = $yesterday = date('Y-m-d H:i:s', time());;
    $query = \Drupal::entityQuery('webform_discount')
      ->condition('code', $code)
      ->condition('expiration', $now, '>' );
    // TODO: Add more conditions - e.g. comes before expiration date. -- Though may want to consider
    // Alternatively, check the time on the loaded code, and provide an error message with context.
    $matches = $query->execute();

    if (!empty($matches)) {
      // Set error if there was a non-empty value, and it didn't match anything.
      $discounts = Entity\WebformDiscount::loadMultiple($matches);
      $this->removeUsageExceededDiscounts($discounts, $code);

      return $discounts;
    }
    else {
      return false;
    }
  }

  /**
   * Gets the usage of individual code.
   * @param $code
   * @return mixed
   */
  protected function getDiscountCodeUsage($code) {
    $database = \Drupal::service('database');
    $select = $database->select('webform_submission_data', 'wsd')
      ->fields('wsd', array('sid'))
      ->condition('wsd.webform_id', $this->getWebform()->id(), '=')
      ->condition('wsd.name', $this->getCodeElementKey(), '=')
      ->condition('wsd.value', $code, '=');
    $executed = $select->execute();
    // Get all the results.
    $results = $executed->fetchAll(\PDO::FETCH_ASSOC);
    if (count($results) == 1) {
      $results = reset($results);
    }
    return $results;
  }

  /**
   * @param array $discounts
   * @param $code
   */
  protected function removeUsageExceededDiscounts(array &$discounts, $code) {
    $toRemove = [];
    $usage = $this->getDiscountCodeUsage($code);
    foreach ($discounts as $delta => $discount) {
      $limit = $discount->getUsageLimit();
      if (count($usage) >= $limit) {
        $toRemove[$delta] = $delta;
      }
    }
    foreach ($toRemove as $remove) {
      unset($discounts[$remove]);
    }
  }

  /**
   * Extracts a value from a WebformSubmissionInterface and applies a discount.
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   * @param $discount
   */
  private function applySubmissionDiscount(WebformSubmissionInterface $webform_submission, $discount) {
    $targetKey = $this->getTargetElementKey();
    $values = $webform_submission->getData();
    // "placeholder" is not the same as value.
    $originalValue = (float) $values[$targetKey];
    $discountValue = $this->applyDiscount($originalValue, $discount);
    return $discountValue;
  }

  /**
   * Applies a discount to a numberical value,
   *
   * @param number $originalValue
   * @param \Drupal\webform_discount\Entity\WebformDiscount $discount
   * @return string a formatted number with two decimal places.
   */
  public function applyDiscount($originalValue, $discount){
    if (strpos($originalValue, '$') !== FALSE) {
      $originalValue = str_replace('$', '', $originalValue);
    }
    $discountValue = $discount->applyDiscount($originalValue);
    return number_format($discountValue, 2);
  }
}
