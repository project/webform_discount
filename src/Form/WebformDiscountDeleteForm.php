<?php

namespace Drupal\webform_discount\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Webform discount entities.
 *
 * @ingroup webform_discount
 */
class WebformDiscountDeleteForm extends ContentEntityDeleteForm {


}
