<?php

namespace Drupal\webform_discount\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a Webform discount revision.
 *
 * @ingroup webform_discount
 */
class WebformDiscountRevisionDeleteForm extends ConfirmFormBase {

  /**
   * The Webform discount revision.
   *
   * @var \Drupal\webform_discount\Entity\WebformDiscountInterface
   */
  protected $revision;

  /**
   * The Webform discount storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $webformDiscountStorage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->webformDiscountStorage = $container->get('entity_type.manager')->getStorage('webform_discount');
    $instance->connection = $container->get('database');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webform_discount_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the revision from %revision-date?', [
      '%revision-date' => format_date($this->revision->getRevisionCreationTime()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.webform_discount.version_history', ['webform_discount' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $webform_discount_revision = NULL) {
    $this->revision = $this->WebformDiscountStorage->loadRevision($webform_discount_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->WebformDiscountStorage->deleteRevision($this->revision->getRevisionId());

    $this->logger('content')->notice('Webform discount: deleted %title revision %revision.', ['%title' => $this->revision->label(), '%revision' => $this->revision->getRevisionId()]);
    $this->messenger()->addMessage(t('Revision from %revision-date of Webform discount %title has been deleted.', ['%revision-date' => format_date($this->revision->getRevisionCreationTime()), '%title' => $this->revision->label()]));
    $form_state->setRedirect(
      'entity.webform_discount.canonical',
       ['webform_discount' => $this->revision->id()]
    );
    if ($this->connection->query('SELECT COUNT(DISTINCT vid) FROM {webform_discount_field_revision} WHERE id = :id', [':id' => $this->revision->id()])->fetchField() > 1) {
      $form_state->setRedirect(
        'entity.webform_discount.version_history',
         ['webform_discount' => $this->revision->id()]
      );
    }
  }

}
