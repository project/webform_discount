<?php

namespace Drupal\webform_discount;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\webform_discount\Entity\WebformDiscountInterface;

/**
 * Defines the storage handler class for Webform discount entities.
 *
 * This extends the base storage class, adding required special handling for
 * Webform discount entities.
 *
 * @ingroup webform_discount
 */
interface WebformDiscountStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Webform discount revision IDs for a specific Webform discount.
   *
   * @param \Drupal\webform_discount\Entity\WebformDiscountInterface $entity
   *   The Webform discount entity.
   *
   * @return int[]
   *   Webform discount revision IDs (in ascending order).
   */
  public function revisionIds(WebformDiscountInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Webform discount author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Webform discount revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

}
