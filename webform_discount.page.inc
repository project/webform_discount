<?php

/**
 * @file
 * Contains webform_discount.page.inc.
 *
 * Page callback for Webform discount entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Webform discount templates.
 *
 * Default template: webform_discount.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_webform_discount(array &$variables) {
  // Fetch WebformDiscount Entity Object.
  $webform_discount = $variables['elements']['#webform_discount'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
